;;; 3x0ng.lisp --- peach puff's underwater adventure

;; Copyright (C) 2010, 2011, 2012, 2013  David O'Toole

;; Author: David O'Toole <dto@ioforms.org>
;; Keywords: games

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :3x0ng)

(defparameter *version-string* "3x0ng v0.1")

(eval-when (:load-toplevel) 
  (setf *window-title* *version-string*)
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)
  (setf *current-directory*
	(make-pathname
	 :directory (pathname-directory #.#P"./"))))

;;; Main program

(defun 3x0ng (&optional (level 1))
  (setf *level* level)
  (setf *retries* *initial-retries*)
  (setf *window-title* *version-string*)
  (setf *screen-width* 1280)
  (setf *screen-height* 720)
  (setf *nominal-screen-width* 1280)
  (setf *nominal-screen-height* 720)
  ;; zoomout 
;;  (setf *nominal-screen-width* (* 1280 5))
;;  (setf *nominal-screen-height* (* 720 5))
  ;;
  (setf *scale-output-to-window* t) 
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)

  (setf *frame-rate* 30)
  
  (disable-key-repeat) 
  
  (with-session 
      (load-project "3x0ng" '(:with-database nil))

    ;; (setf *preload-images* t)
    ;; (setf *preload-samples* t)
    (index-pending-resources)
    ;;    (preload-resources)

    ;; (setf *soundtrack* (derange *soundtrack*))
    (switch-to-buffer (3x0ng-level))
    ;; (bind-event (current-buffer)  '(:space) :start-playing)
    (start-session)))

(define-buffer 3x0ng
  (layered :initform nil)
  (bubble :initform nil)
  (retrying :initform nil)
  (default-events 
     :initform
     '(((:r :control) :reset-game)
       ((:q :control) :quit-game)
       ((:y :control) :show-waypoint)
       ((:h :control) :help)
       ((:m :control) :toggle-music) 
       ((:p :control) :toggle-pause)
       ((:j :control) :toggle-joystick)
       ;;       ((:f8) :cheat)
       ((:n :control) :next-joystick)
       ;;
       ((:x :alt) :command-prompt)
       ((:g :control) :escape)
       ((:f6 :control) :regenerate)
       ;;
       ((:x :alt) :command-prompt)
       ((:x :control) :edit-cut)
       ((:c :control) :edit-copy)
       ((:v :control) :edit-paste)
       ((:v :control :shift) :paste-here)
       ((:f9) :toggle-minibuffer)
       ((:f12) :transport-toggle-play)
       ((:g :control) :escape)
       ((:d :control) :drop-selection))))

(define-method cheat 3x0ng ()
  (room)
  (next-level))

;;; Disable mouse editing

(defun set-buffer-bubble (bubble)
  (setf (field-value :bubble (current-buffer)) bubble))

;; (define-method handle-point-motion 3x0ng (x y))
;; (define-method press 3x0ng (x y &optional button))
;; (define-method release 3x0ng (x y &optional button))
;; (define-method tap 3x0ng (x y))
;; (define-method alternate-tap 3x0ng (x y))

(define-method quit-game 3x0ng ()
  (at-next-update (xelf:quit t)))

;;; Various toggles

(defvar *music-toggled* nil)

;; (define-method toggle-music 3x0ng ()
;;   (setf *music-toggled* t)
;;   (if (sdl-mixer:music-playing-p)
;;       (halt-music)
;;       (play-music (random-choose *soundtrack*) :loop t)))

(define-method help 3x0ng () 
  (switch-to-buffer (help-buffer)))

(define-method next-joystick 3x0ng ()
  (let ((n (number-of-joysticks))
	(i *joystick-device-number*))
    (reset-joystick (mod (1+ i) n))
    (drop (cursor) 
	  (new 'bubble :text (format nil "Choosing joystick number ~D" *joystick-device-number*)))))

(define-method regenerate 3x0ng () (reset-level))

(define-method toggle-joystick 3x0ng ()
  (setf *joystick-enabled* (if *joystick-enabled* nil t))
  (drop (cursor) 
	(new 'bubble :text
	     (if *joystick-enabled* 
		 "Joystick support on."
		 "Joystick support off."))))
      
(define-method toggle-pause 3x0ng ()
  (when (not %paused)
    (drop (cursor) 
	  (new 'bubble :text 
	       "Game paused. Press Control-P to resume play.")))
  (transport-toggle-play self)
  (when (not %paused)
    (loop for thing being the hash-keys of %objects do
      (when (bubblep thing) (destroy (find-object thing))))))

(define-method reset-game 3x0ng (&optional (level 1))
  (setf *retries* *initial-retries*)
  (begin-game level))

(define-method update 3x0ng ()
  (buffer%update self)
  (when %retrying (begin-game *level*)))

(define-method show-waypoint 3x0ng ()
  (drop-waypoint-maybe (cursor) :force))

(define-method draw 3x0ng ()
  (buffer%draw self)
  (when (xelfp %bubble) (draw %bubble)))

;;; 3x0ng.lisp ends here
