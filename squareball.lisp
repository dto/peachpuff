(in-package :3x0ng)

(defresource "error.wav" :volume 60)

(defresource 
      (:name "boop1.wav" :type :sample :file "boop1.wav" :properties (:volume 20))
      (:name "boop2.wav" :type :sample :file "boop2.wav" :properties (:volume 20))
    (:name "boop3.wav" :type :sample :file "boop3.wav" :properties (:volume 20)))

(defparameter *bounce-sounds* '("boop1.wav" "boop2.wav" "boop3.wav"))

(defresource 
    (:name "doorbell1.wav" :type :sample :file "doorbell1.wav" :properties (:volume 23))
    (:name "doorbell2.wav" :type :sample :file "doorbell2.wav" :properties (:volume 23))
  (:name "doorbell3.wav" :type :sample :file "doorbell3.wav" :properties (:volume 23)))

(defparameter *doorbell-sounds* '("doorbell1.wav" "doorbell2.wav" "doorbell3.wav"))

(defparameter *slam-sounds*
  (defresource 
      (:name "slam1.wav" :type :sample :file "slam1.wav" :properties (:volume 52))
      (:name "slam2.wav" :type :sample :file "slam2.wav" :properties (:volume 52))
    (:name "slam3.wav" :type :sample :file "slam3.wav" :properties (:volume 52))))

(defresource 
    (:name "whack1.wav" :type :sample :file "whack1.wav" :properties (:volume 82))
    (:name "whack2.wav" :type :sample :file "whack2.wav" :properties (:volume 82))
  (:name "whack3.wav" :type :sample :file "whack3.wav" :properties (:volume 82)))

(defparameter *whack-sounds* '("whack1.wav" "whack2.wav" "whack3.wav"))

(defresource 
    (:name "color1.wav" :type :sample :file "color1.wav" :properties (:volume 32))
    (:name "color2.wav" :type :sample :file "color2.wav" :properties (:volume 32))
  (:name "color3.wav" :type :sample :file "color3.wav" :properties (:volume 32)))

(defparameter *color-sounds* '("color1.wav" "color2.wav" "color3.wav"))

;;; The Squareball

(defvar *ball* nil)

(defvar *auto-return-distance* 520)

(defparameter *ball-size* (truncate (units 1.2)))

(defun ballp (thing)
  (and (xelfp thing)
       (has-tag thing :ball)))

(defparameter *ball-normal-speed* (units 0.88))

(defparameter *ball-kick-speed* (units 1.16))

(defparameter *ball-deceleration* (units 0.0))

(defblock ball 
  :kicker nil
  :target nil
  :target-distance nil
  :kicker-distance nil
  :last-target nil
  :last-target-hits 0
  :seeking nil
  :height *ball-size* :width *ball-size*
  :color "white"
  :speed 0
  :bounces 20
  :hits 7
  :kick-clock 0 :tags '(:ball :colored))

(define-method paint ball (color)
  ;; beep when color changes
  (when (not (string= %color color))
    (play-sample (random-choose *color-sounds*)))
  (setf %color color))

(defmethod initialize ((self ball) &key color)
  (setf *ball* self)
  (resize self *ball-size* *ball-size*)
  (when color (setf (field-value %color self) color)))

(defun make-ball (&optional (color "white"))
  (setf *ball* (new 'ball color)))

(define-method after-deserialize ball ()
  (setf *ball* self))

(defresource "bounce.wav" :volume 10)
(defresource "newball.wav" :volume 20)
(defresource "return.wav" :volume 20)

(define-method explode ball ()
  (make-sparks %x %y "white")
  (setf *ball* nil)
  (paint %kicker %color)
  (play-sample "error.wav")
  (destroy self))

(define-method bounce ball ()
  (decf %bounces)
  (if (or (zerop %bounces)
	  ;; keep from leaving map
	  (not (bounding-box-contains 
	  	(multiple-value-list (bounding-box (current-buffer)))
	  	(multiple-value-list (bounding-box self)))))
      (explode self)
      (progn
	(play-sound self (random-choose *bounce-sounds*))
	(unless (zerop %last-x)
	  (restore-location self))
	(setf %heading 
	      (if %seeking
		  (opposite-heading %heading)
		  (opposite-heading %heading)))
	(move self %heading 2.2)
	(setf %kick-clock 0)
	(setf %seeking (if %seeking nil t)))))
      
(define-method find-enemy ball (thing2 &optional (range (coerce 180 'single-float)))
  (let ((enemies
	  (loop for thing being the hash-values of (%objects (current-buffer))
		when (and 
		      (xelfp thing)
		      (enemyp thing)
		      (has-tag thing :target)
		      (not (object-eq thing2 thing))
		      (colliding-with-rectangle thing 
						(- %y range)
						(- %x range)
						(* 2 range)
						(* 2 range)))
		  collect thing)))
    (when (consp enemies)
      (first enemies))))

(define-method update ball ()
  (when (not (xelfp %target)) 
    (setf %target nil)
    (setf %target-distance nil))
  (when (plusp %kick-clock)
    (decf %kick-clock))
  (with-fields (seeking heading speed kicker) self
    (when (> (distance-between self kicker) *auto-return-distance*)
      (when (not %seeking) (play-sample "return.wav"))
      (setf %seeking t))
    (if (plusp speed)
	(if %target 
	    (progn
	      (setf %target-distance (distance-between self %target))
	      (move self (heading-to-thing2 self %target) speed)
	      ;; check if distance is not decreasing
	      (when (and %target-distance
			 (< %target-distance (distance-between self %target)))
		(when (has-method :damage %target)
		  (damage %target 1))))
	    (if seeking
		;; return to player
		(progn
		  (setf %kicker-distance (distance-between self kicker))
		  (move self (heading-to-thing self kicker) speed)
		  ;; return to kicker if distance did not decrease
		  (when (and %kicker-distance
			     (< %kicker-distance (distance-between self kicker)))
		    (multiple-value-bind (x y) (center-point kicker)
		      (move-to self x y))))
		;; just move forward
		(move self heading speed)))
	(setf %seeking nil))))

(define-method recently-kicked-by ball (cursor)
  (and (plusp %kick-clock) 
       (object-eq cursor %kicker)))
    
(define-method collide ball (thing)
  ;;
  (cond 
    ;; bonk enemies
    ((enemyp thing)
     (decf %hits)
     (when (zerop %hits) (setf %seeking t))
     (when (has-method :bonk thing)
       (bonk thing (- 0 (opposite-heading %heading))))
     (bounce self))
    ;; stop at person unless it's the guy who just kicked it.
    ;; helps avoid ball getting stuck.
    ((and (personp thing) (humanp thing))
     ;; player catches ball
     (when (not (recently-kicked-by self thing))
       (paint thing (color-of *ball*))
       (destroy self)
       (setf *ball* nil)))
    ;; enemy AI catches ball 
    ;; ((and (personp thing) (not (humanp thing)))
    ;;
    ;; bounce off corals
    ((coralp thing)
     (slap thing)
     (bounce self))))

(defparameter *ball-kicker-collision-delay* 4)

(define-method impel ball (heading strong &optional kicker)
  (setf %kicker kicker)
  (setf %kick-clock *ball-kicker-collision-delay*)
  (setf %seeking nil)
  (setf %speed (if strong *ball-kick-speed* *ball-normal-speed*))
  (setf %heading heading))

(define-method draw ball ()
  (with-field-values (x y width height color) self
    (draw-box x y width height :color "white")
    (draw-box (+ 2 x) (+ 2 y) (- width 4) (- height 4) :color color)))

