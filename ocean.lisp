(in-package :3x0ng)

  ;;; Deadly burning gas clouds

(defresource 
    (:name "geiger1.wav" :type :sample :file "geiger1.wav" :properties (:volume 10))
    (:name "geiger2.wav" :type :sample :file "geiger2.wav" :properties (:volume 10))
  (:name "geiger3.wav" :type :sample :file "geiger3.wav" :properties (:volume 10))
  (:name "geiger4.wav" :type :sample :file "geiger4.wav" :properties (:volume 10)))

(defparameter *vent-sounds* '("geiger1.wav" "geiger2.wav" "geiger3.wav" "geiger4.wav"))

(defresource 
    (:name "vent.png" :type :image :file "vent.png")
    (:name "vent2.png" :type :image :file "vent2.png")
  (:name "vent3.png" :type :image :file "vent3.png")
  (:name "vent4.png" :type :image :file "vent4.png")
  (:name "vent5.png" :type :image :file "vent5.png"))

(defparameter *vent-images* '("vent.png" "vent2.png" "vent3.png" "vent4.png" "vent5.png"))

(defblock cloud 
  :timer 230
  :collision-type :passive
  :tags '(:cloud)
  :image "vent.png")

(defun cloudp (thing)
  (and (xelfp thing)
       (has-tag thing :cloud)))

(define-method draw cloud ()
  (with-field-values (x y width height image) self
    (let ((jitter (random 10)))
      (when (> jitter 7)
	(incf %heading (random-choose '(-0.3 0.2))))
      (set-blending-mode :additive2)
      (draw-image image
		  (- x jitter)
		  (- y jitter)
		  :width (+ width jitter)
		  :height (+ height jitter)
		  :opacity 1)
      (dotimes (n 2) 
	(draw-box (+ x -5 (random height))
		  (+ y -5 (random width))
		  (+ 5 (random 8))
		  (+ 5 (random 8))
		  :color (random-choose '("white" "magenta"))
		  :alpha 0.7)))))

(defmethod initialize ((self cloud) &key (size (+ 16 (random 32))))
  (initialize%super self)
  (resize self size size))

(define-method update cloud ()
  (forward self 1)
  (decf %timer)
  (when (evenp %timer)
    (setf %image (random-choose *vent-images*)))
  (when (< (distance-to-cursor self) 300)
    (percent-of-time 8 (play-sample (random-choose *vent-sounds*))))
  (unless (plusp %timer)
    (destroy self)))

(defresource 
    (:name "vent-hole1.png" :type :image :file "vent-hole1.png")
    (:name "vent-hole2.png" :type :image :file "vent-hole2.png")
  (:name "vent-hole3.png" :type :image :file "vent-hole3.png"))

(defparameter *vent-hole-images* '("vent-hole1.png" "vent-hole2.png" "vent-hole3.png"))

(defblock vent 
  :image "vent-hole1.png" :tags '(:enemy :vent) 
  :timer 0)

(define-method update vent ()
  (percent-of-time 12 (setf %image (random-choose *vent-hole-images*)))
  (with-fields (timer) self 
    (when (plusp timer)
      (decf timer))
    (when (zerop timer)
      (percent-of-time 3
	(when (< (distance-to-cursor self) 350)
	  (setf timer 20))))
    (percent-of-time 0.3
      (drop self (new 'cloud) 40 40))))

(define-method damage vent (points) nil)

(define-method collide vent (thing)
  (when (personp thing)
    (die thing)))

;;; Assembling it all

(defvar *extra-padding* 0)
  
(defun with-hpadding (amount buffer)
  (with-field-values (height width) buffer
    (with-new-buffer 
      (paste-into (current-buffer) buffer amount 0) 
      (resize (current-buffer)
	      height
	      (+ width amount *extra-padding*)))))


(defun with-vpadding (amount buffer)
  (with-field-values (height width) buffer
    (with-new-buffer 
      (paste-into (current-buffer) buffer 0 amount) 
      (resize (current-buffer)
	      (+ height amount *extra-padding*)
	      width))))

(defun with-automatic-padding (buffer)
  (with-border (units 2)
    (let ((padding (+ (units 1) 
		      (random (units 25)))))
      (if (evenp *depth*)
	  (with-hpadding padding buffer)
	  (with-vpadding padding buffer)))))

(defun horizontally (a b)
  (percent-of-time 50 (rotatef a b))
  (arrange-beside 
   (with-border 10 a)
   (with-border 10 b)))

(defun vertically (a b)
  (percent-of-time 50 (rotatef a b))
  (arrange-below 
   (with-border 10 a)
   (with-border 10 b)))

(defun either-way (a b)
  (funcall (or (percent-of-time 50 #'horizontally) #'vertically)
	   a b))

(defun bordered (x) 
  (assert (xelfp x))
  (with-border 30 x))

(defun singleton (x) 
  (assert (xelfp x))
  (bordered (with-new-buffer (drop-object (current-buffer) x) (trim (current-buffer)))))

(defun stacked-up (&rest things)
  (assert things)
  (if (= 1 (length things))
      (first things)
      (arrange-below (first things) (apply #'stacked-up (rest things)))))

(defun lined-up (&rest things)
  (assert things)
  (if (= 1 (length things))
      (first things)
      (arrange-beside (first things) (apply #'lined-up (rest things)))))

(defun stacked-up-randomly (&rest things)
  (bordered (apply #'funcall #'stacked-up (derange things))))

(defun lined-up-randomly (&rest things)
  (bordered (apply #'funcall #'lined-up (derange things))))

(defun randomly (&rest things)
  (apply #'funcall (or (percent-of-time 50 #'stacked-up-randomly)
		       #'lined-up-randomly) 
	 things))

;; Making walls

(defun wall-at (x y width height &optional (thing 'wall))
  (labels ((unit (&rest summands)
	     (* *unit* (reduce #'+ summands :initial-value 0))))
    (let ((wall (new thing *wall-color*)))
      (drop-object (current-buffer) wall (unit x) (unit y))
      (resize wall (unit width) (unit height)))))

(defun wall-around-region (x y width height)
    (let ((left x)
	  (top y)
	  (right (+ x width))
	  (bottom (+ y height)))
      ;; top wall
      (wall-at left top (- right left) 1)
      ;; bottom wall
      (wall-at left bottom (- right left -1) 1)
      ;; left wall
      (wall-at left top 1 (- bottom top))
      ;; right wall
      (wall-at right top 1 (- bottom top -1))))

;;; Assembly

(defun pad-to-window (buffer)
  (prog1 buffer
    (with-fields (width height) buffer
      (setf width (max width *screen-width*))
      (setf height (max height *screen-height*)))))

(defun make-puzzle ()
  (with-new-buffer 
;;    (insert (new 'vent))
    (dotimes (n 8)
      (drop-object (current-buffer) (new 'jelly)
		   (+ 200 (random 800))
		   (+ 200 (random 800))))
    (current-buffer)))

(defun background-color () (random-choose '("black")))

(defresource "ocean.png")

(defun background-image () "ocean.png")

(defun begin-game (level)   
  (let ((old-buffer (current-buffer)))
    (switch-to-buffer (3x0ng-level))
    (at-next-update (destroy old-buffer))))
;;    (play-sample "newball.wav")))

(defun 3x0ng-level (&optional (level 1))
  (setf *ball* nil)
  (let ((peep (new 'player-1-peep "PeachPuff"))
	(buffer (new '3x0ng))
	(puzzle (pad-to-window
		 (with-border (units 16)
		   (make-puzzle)))))
    (with-buffer buffer
      (setf (field-value :background-color (current-buffer)) (background-color))
      (setf (field-value :background-image (current-buffer)) (background-image))
      (paste-from buffer puzzle)
      ;; playfield border
      ;; (wall-around-region -1 2 
      ;; 			  (+ 8 (truncate (/ (%width puzzle) (units 1))))
      ;; 			  (+ 8 (1- (truncate (/ (%height puzzle)
      ;; 						 (units 1))))))
      (destroy puzzle)
      ;; adjust scrolling parameters 
      (setf (%window-scrolling-speed buffer) (/ *peep-speed* 2)
	    (%horizontal-scrolling-margin buffer) 2/5
	    (%vertical-scrolling-margin buffer) 4/7)
      ;;
      (trim (current-buffer))
      ;; player 1 always goes top left on level 1
      (if (or (= *level* 1)
	      ;; otherwise, randomly choose a corner
	      (percent-of-time 50 t))
	  (drop-object (current-buffer) peep (units 4) (units 3.4))
	  (drop-object (current-buffer) peep 
		       (- (%width (current-buffer))
			  (units 12)) 
		       (- (%height (current-buffer))
			  (units 8))))
      (resize-to-background-image (current-buffer))
      (set-cursor (current-buffer) peep)
      (snap-window-to-cursor (current-buffer))
      (glide-window-to-cursor (current-buffer))
      ;; allocate
      (install-quadtree (current-buffer))
      (follow-with-camera (current-buffer) peep)
      (current-buffer))))

