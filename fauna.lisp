(in-package :3x0ng)

;;; Basic enemy

(defblock enemy 
  :image "blue-fish-right.png" 
  :heading (random (* 2 pi))
  :px 0 :py 0 :pz 0
  :x0 0 :y0 0 :z0 0
  :ballistic nil
  :interpolation :linear
  :frames 0
  :timer 0)

(defmacro define-enemy (name &body body)
  `(defblock (,name :super enemy)
     ,@body))

(defparameter *swoop-frames* 80)

(define-method glide-to enemy (x y &key (interpolation :linear) (timer *swoop-frames*))
  (setf %ballistic t)
  (setf %px %x %py %y)
  ;; (setf %x0 x %y0 y)
  (setf %x0 (- x (/ %width 2))
	%y0 (- y (/ %height 2)))
  (setf %interpolation interpolation)
  (setf %timer timer %frames timer))

(defun interpolate (px x a &optional (interpolation :linear))
  (let ((range (- x px)))
    (float 
     (+ px
	(ecase interpolation
	  (:linear (* a range))
	  (:sine (* (sin (* a (/ pi 2))) range)))))))

(define-method update-physics enemy ()
  (with-fields (ballistic x0 y0 px py frames timer interpolation) self
    (if (zerop timer)
	(setf ballistic nil)
	;; scale to [0,1]
	(let ((a (/ (- timer frames) frames)))
	  (move-to self
		   (interpolate px x0 a interpolation)
		   (interpolate py y0 a interpolation))
	  (decf timer)))))

(define-method cancel-physics enemy ()
  (setf %timer 0)
  (setf %ballistic nil))

(define-method run enemy ())

(define-method update enemy ()
  (update-physics self)
  (run self))

(define-method bonk enemy (heading)
  (setf %heading heading)
  (multiple-value-bind (x y)
      (step-toward-heading self %heading 300)
    (glide-to self x y :interpolation :sine :timer 40)))

(defresource "xplod.wav" :volume 80)

;;; The "jelly", a roving horror Jellypuss entity

(defresource "blue-fish-right.png")
(defresource "blue-fish-left.png")
(defresource "pink-fish-right.png")
(defresource "pink-fish-left.png")

(define-enemy jelly 
  (direction :initform (random-direction))
  (hp :initform 3) 
  (tags :initform '(:jelly :enemy :target))
  (image :initform 
	 (random-choose '("pink-fish-left.png" "pink-fish-right.png" "blue-fish-left.png" "blue-fish-right.png"))))

(define-method hunt jelly ()
  (let ((dist (distance-to-cursor self)))
    ;; hunt for player
    (if (< dist (with-difficulty 350 360 370 380 420))
	(progn 
	  (setf %heading (heading-to-cursor self))
	  (forward self (with-difficulty 1.6 1.8 2.0 2.1 2.2 2.3)))
	;; patrol
	(progn (percent-of-time 1 (setf %direction (random-direction)))
	       (move-toward self %direction (with-difficulty 1 2 2 2.5 2.5 3))))))

(defresource "grow.wav" :volume 10)
(defresource "grow2.wav" :volume 10)

(define-method run jelly ()
  (unless %ballistic (hunt self)))

(define-method collide jelly (thing)
  (when (not (enemyp thing))
    (restore-location self))
  (when (personp thing) (die thing))
  (setf %direction (random-direction)))

(define-method damage jelly (points)
  (decf %hp)
  (play-sound self (random-choose *whack-sounds*))
  (when (zerop %hp)
    (multiple-value-bind (x y) (center-point self)
      (let ((size (truncate (/ %width (units 1)))))
	(when (> size 2)
	  (make-sparks x y "magenta")))
      (make-sparks x y "yellow")
      (play-sound self "xplod.wav")
      (destroy self))))

(define-method fire jelly (heading)
  (multiple-value-bind (x y) (center-point self)
    (drop self (new 'bullet heading :timer 40))))

;;; Stupid shark

(define-enemy shark :tags '(:enemy :shark) :heading (random-choose (list pi 0.0)))

(defmethod initialize ((self shark) &key)
  (resize self 60 12))

(define-method draw shark ()
  (draw-box %x %y %width %height :color (random-choose '("magenta" "deep pink" "hot pink"))))

(define-method update shark ()
  (let ((speed
	  (if (< (distance-to-cursor self)
		 (with-difficulty 200 200 250 250 300 300 350))
	      (with-difficulty 8 8.5 9 9.5 10 10.5 11 11.5 12 12.5 13 13.5) 
	      (with-difficulty 2 2 3 3 4 4 5 6 7 8))))
    (forward self speed)))

(define-method collide shark (thing)
  (when (brickp thing)
    (restore-location self)
    (setf %heading (- %heading pi))))

(define-method damage shark (thing) nil)

