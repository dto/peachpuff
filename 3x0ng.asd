(asdf:defsystem #:3x0ng
  :depends-on (:xelf)
  :components ((:file "package")
	       (:file "basic" :depends-on ("package"))
	       (:file "squareball" :depends-on ("basic"))
	       (:file "peeps" :depends-on ("squareball"))
	       (:file "fauna" :depends-on ("peeps"))
	       (:file "ocean" :depends-on ("fauna"))
	       (:file "3x0ng" :depends-on ("ocean"))))
