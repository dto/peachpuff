(in-package :3x0ng)

(defparameter *initial-retries* 2)

(defparameter *retries* *initial-retries*)

(defparameter *unit* 20)

(defun units (n) (* n *unit*))

(defparameter *level* 0)

(defparameter *difficulty* 0)

(defun with-difficulty (&rest args)
  (if (<= (length args) *difficulty*)
      (nth (1- (length args)) args)
      (nth *difficulty* args)))

(defun targetp (thing)
  (and (xelfp thing)
       (has-tag thing :target)))

(defun enemyp (thing)
  (and (xelfp thing)
       (has-tag thing :enemy)))

(defparameter *depth* 0)

(defun with-depth (&rest args)
  (if (<= (length args) *depth*)
      (nth (1- (length args)) args)
      (nth *depth* args)))

;;; Sparkle explosion cloud fx

(defblock spark 
  :tags '(:spark)
  :width 3 :height 3 :color nil
  :collision-type :passive)

(defun sparkp (thing)
  (has-tag thing :spark))

;; DISABLED
(define-method collide spark (thing)
  (when (and (enemyp thing) (has-method :damage thing))
    (damage thing 1)
    (destroy self)))

(defmethod initialize ((self spark) &key (color "white"))
  (setf (field-value :color self) color)
  (later 0.3 (destroy self)))

(define-method draw spark ()
  (set-blending-mode :additive)
  (with-field-values (x y color) self
    (dotimes (n 8)
      (let ((z (+ 2 (random 4))))
	(draw-box (+ x (- 20 (random 40)))
		  (+ y (- 20 (random 40)))
		  z z
		  :color (random-choose '("cyan" "magenta"))))))
  (set-blending-mode :alpha))

(define-method update spark ()
  (move-toward self (random-direction) (+ 8 (random 8))))

(defun make-sparks (x y &optional color)
  (dotimes (z 5)
    (drop-object (current-buffer)
		 (new 'spark color) 
		 (+ x (random 30)) (+ y (random 30)))))

;; Coral reef

(defblock coral 
  :tags '(:coral :colored)
  :hits 10
  :collision-type :passive
  :color "gray50")

(define-method damage coral (points) 
  (multiple-value-bind (x y) (center-point self)
    (make-sparks x y %color)
    (destroy self)))

(defmethod initialize ((self coral) &key (color "white"))
  (resize self (units 3) (units 2))
  (when color (setf (field-value :color color) color)))

(define-method draw coral ()
  (with-fields (x y z width height color) self
    ;; (let ((hash (color-hash color)))
      (set-blending-mode :alpha)
      ;; (if (and *red-green-color-blindness* hash)
      ;; 	  (draw-textured-rectangle x y z width height
      ;; 				   (find-texture (solid-hash-image hash))
      ;; 				   :vertex-color color)
	  (draw-box x y width height :color color)))

(define-method decay coral ()
  (decf %hits)
  (unless (plusp %hits)
    (damage self 1)))

(defun slap (thing)
  (when (and (xelfp thing)
	     (has-method :damage thing))
    (damage thing 1)))

(defun coralp (thing)
  (and (xelfp thing)
       (has-tag thing :coral)))

(defun coloredp (thing)
  (and (xelfp thing)
       (has-tag thing :colored)))

(defun color-of (thing)
  (or (when (coloredp thing)
	(%color thing))
      "white"))

(define-method paint coral (color)
  (when (coloredp self)
    (setf %color color)))

(defun same-color (a b)
  (string= (color-of a) (color-of b)))

(defun personp (thing)
  (and (xelfp thing)
       (has-tag thing :peep)))

(defun bubblep (thing)
  (and (xelfp thing)
       (has-tag thing :bubble)))

;;; A non-breakable coral stone

(defparameter *wall-color* "gray50")

(defblock (wall :super coral)
  (tags :initform '(:coral :wall))
  (color :initform *wall-color*))

(defmethod initialize ((self wall) &key width height)
  (when (and (numberp height) (numberp width))
    (resize self width height)))

(defun wallp (thing)
  (and (xelfp thing)
       (has-tag thing :wall)))

(define-method damage wall (points) 
  ;; TODO flash and/or make a cool (themeable) sound 
  nil)

;;; Dialogue bubble

(defblock bubble 
  (tags :initform '(:bubble))
  (font :initform nil)
  (text :initform nil) 
  (collision-type :initform nil))

(defmethod initialize ((self bubble) &key (font "sans-mono-12"))
  (with-local-fields 
      (setf %text text)
    (setf %font font)
    (set-buffer-bubble self)
    (later 12.0 (destroy self))))

(define-method destroy bubble ()
  (set-buffer-bubble nil)
  (block%destroy self))

(define-method draw bubble ()
  (multiple-value-bind (top left right bottom)
      (window-bounding-box (current-buffer))
    (draw-box left top (- right left) (units 2) :color "black")
    (draw-string %text 
		 (+ left (units 1))
		 (+ top (units 0.4))
		 :color (random-choose '("cyan" "white"))
		 :font %font)))

(define-method update bubble ()
  (when (cursor)
    (multiple-value-bind (x y)
	(right-of (cursor))
      (move-to self x y))))

